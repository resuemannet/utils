import sys
import praw

reddit = praw.Reddit(client_id="y1o3JKxzA4XqTQ",
                     client_secret="9P3ZeUiJYJAGM-CzwSG2K4OJcJw",
                     user_agent="SPS_searcher:v0.0.1 (by /u/resueman__)",
                     username="<USERNAME>",
                     password="<PASSWORD>")

def isTextPost(post):
    return post.is_self

def buildSubmissionCommenterList(rootSubmission):
    contents = dict()
    for comment in rootSubmission.comments:
        buildCommenterList(comment, contents)

    return contents

def buildCommenterList(rootComment, contents = dict()):
    if isinstance(rootComment, praw.models.MoreComments):
        for comment in rootComment.comments():
            buildCommenterList(comment, contents)
        return contents


    author = ''
    if rootComment.author is not None:
        author = rootComment.author.name

    time = rootComment.created
    if author not in contents or time < contents[author]:
        contents[author] = time

    for reply in rootComment.replies:
        if reply is not None:
            buildCommenterList(reply, contents)
    return contents

def isWholeCommentSection(post):
    try:
        linked = praw.models.Comment(reddit, url=post.url)
    except praw.exceptions.ClientException:
        return True
    return False

def getBrigaders(submission):
    if isTextPost(submission):
        return ([], [])

    submitters = buildSubmissionCommenterList(submission)
    linkedSubmitters = dict()

    try:
        linkedComment = praw.models.Comment(reddit, url=submission.url)
        linkedSubmitters = buildCommenterList(linkedComment)
    except:
        linkedSubmission = praw.models.Submission(reddit, url=submission.url)
        linkedSubmitters = buildSubmissionCommenterList(linkedSubmission)

    brigaders = []
    suspicious = []

    for submitter in submitters:
        if submitter in linkedSubmitters:
            if submitter is not None and submitter != '':
                if submitters[submitter] < linkedSubmitters[submitter]:
                    brigaders.append(submitter)
                else:
                    suspicious.append(submitter)

    return (brigaders, suspicious)

def printBrigaders(brigaders, suspicious):
    print("Brigaders:")
    for brigader in brigaders:
        print("\t" + brigader)

    print()
    print("Visitors:")
    for visitor in suspicious:
        print("\t" + visitor)

posts = []

if len(sys.argv) > 1:
    for i in range(1, len(sys.argv)):
        spsUrl = sys.argv[1]
        posts.append(praw.models.Submission(reddit, url=spsUrl))
else:
    posts = reddit.subreddit('shitpoliticssays').top('day', limit = 10)
    #posts = reddit.subreddit('shitpoliticssays').hot(limit = 10)
    #posts = reddit.subreddit('shitpoliticssays').new(limit = 10)

for post in posts:
    (brigaders, suspicious) = getBrigaders(post)
    print(post.permalink)
    if len(brigaders) > 0 or len(suspicious) > 0:
        printBrigaders(brigaders, suspicious)
    else:
        print("Normal")
    print("\n\n-------------------------------------------------------------------------------------------------")

